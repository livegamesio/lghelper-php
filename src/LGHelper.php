<?php

namespace LiveGames;

if (!class_exists('\Firebase\JWT\JWT')) {
	require_once("lib/jwt/BeforeValidException.php");
	require_once("lib/jwt/ExpiredException.php");
	require_once("lib/jwt/SignatureInvalidException.php");
	require_once("lib/jwt/JWT.php");
}

use \Firebase\JWT\JWT;

class LGHelper {

	public $expire = 86400;

	private $config = [];

	private $apiKey = "";
	private $apiSecret = "";

	public $lastError = null;

	public $embedTemplate = "";

	function __construct($config = []) {
		$this->config = $config;
		if(array_key_exists("leeway", $config)){
			JWT::$leeway = $config["leeway"];
		}
		return $this;
	}

	private function getConfig($key="", $defaultValue = null) {
		return array_key_exists($key, $this->config) ? $this->config[$key] : $defaultValue;
	}

	public function parseStringToJWT($stringParams = "") {
		parse_str($stringParams, $outParams);
		return $this->createJWT($outParams);
	}

	public function createJWT($claims = [], $returnObject = false) {
		$tm = time();
		$apiKey = array_key_exists("apiKey", $claims) ? $claims["apiKey"] : $this->getConfig("apiKey", "-");
		$apiSecret = array_key_exists("apiSecret", $claims) ? $claims["apiSecret"] : $this->getConfig("apiSecret", "-");
		$userObject = null;

		$jwData = [
			"apiKey" => $apiKey,
			"game" => array_key_exists("game", $claims) ? $claims["game"] : $this->getConfig("game", "tombala"),
			"iat" => $tm,
			"exp" => $tm + (array_key_exists("exp", $claims) ? $claims["exp"] : $this->getConfig("exp", $this->expire)),
			"jti" => hash('crc32', $apiKey . $tm),
		];

		if(array_key_exists("uid", $claims)){
			$jwData = array_merge($jwData, [ "user" => [
				"id" => $claims["uid"],
				"name" => array_key_exists("name", $claims) ? $claims["name"] : $claims["uid"],
				"parent" => array_key_exists("parent", $claims) ? $claims["parent"] : $this->getConfig("parent", "Reseller"),
			]]);
		}
		return ($returnObject) ? $jwData : JWT::encode($jwData, $apiSecret);
	}

	public function callRequest($apiUrl, $q = []) {
		if(strpos($apiUrl,"//") === false){
			$apiUrl = $this->getConfig("apiUrl", "https://api.livegames.io/api/") . $apiUrl;
		}

		if(!array_key_exists("token", $q)){
			$this->lastError = new \Exception("TOKEN_NOT_FOUND");
			return false;
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiUrl);
		curl_setopt($ch, CURLOPT_POST, count($q));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($q));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);

		if ($_SERVER["HTTP_REFERER"]) {
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER["HTTP_REFERER"]);
		} else if ($_SERVER["HTTP_ORIGIN"]) {
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER["HTTP_ORIGIN"]);
		} else if ($_SERVER["HTTP_HOST"]) {
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER["HTTP_HOST"]);
		}
		$response = curl_exec($ch);
		curl_close($ch);
		if (array_key_exists("json", $q) && !($q["json"])) {
			return $response;
		}
		$response = json_decode($response);
	}

	public function buildEIF($config=[])
	{
		$output = false;
		try {

			if(!array_key_exists("frames",$config)){
				throw new \Exception("FRAME_LIST_NOT_EXIST");
			}

			if($this->embedTemplate == ""){
				if(array_key_exists("tpl",$config)){
					$this->embedTemplate = $config["tpl"];
				}else if(array_key_exists("tplFile",$config)){
		    		if (!$config["tplFile"] || !file_exists($config["tplFile"])) {
						throw new \Exception("TEMPLATE_FILE_NOT_FOUND");
		    		}
		    		$this->embedTemplate = file_get_contents($config["tplFile"]);
				}else{
					throw new \Exception("TEMPLATE_NOT_FOUND");
				}
			}
			$output = $this->embedTemplate;

			if(!array_key_exists("currency",$config)){
				$config["currency"] = $this->getConfig("currency", "P");
			}
			if(!array_key_exists("bgColor",$config)){
				$config["bgColor"] = $this->getConfig("bgColor", "transparent");
			}

			$frameList = [];
			foreach ($config["frames"] as $frame) {
		        $frameList[] = "{container:'{$frame["container"]}',windowName :'{$frame["windowName"]}',service : '{$frame["service"]}'}";
		    }
			$frameListStr =  implode(',',$frameList);
			$output = str_replace("[@frameList]",$frameListStr, $output);
			unset($config["frames"]);
			foreach ($config as $key => $value) {
		        $output = str_replace("[@$key]", $value, $output);
		    }
		} catch (Exception $e) {
			$this->lastError = $e;
		}
		return $output;
	}
}
