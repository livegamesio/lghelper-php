<?php
require_once("../src/LGHelper.php");

$lgHelper = new \LiveGames\LGHelper();

//String Parse ---------------------------------------------------
$jwtstring = $lgHelper->parseStringToJWT("uid=USERID123&name=Display Name&parent=Reseller&apiKey=APIKEY&apiSecret=SECRET&game=tombala");
echo $jwtstring . PHP_EOL. PHP_EOL;

//Object Parse ---------------------------------------------------
$tokenObject = [
	"apiKey" => "APIKEY",
	"apiSecret" => "SECRET",
	"game" => "tombala",
	"uid" => "UserId123",
	"name" => "Display Name",
	"parent" => "Reseller",
];
$jwtobjtosting = $lgHelper->createJWT($tokenObject);
echo $jwtobjtosting . PHP_EOL . PHP_EOL;


//Call LiveGames Api ---------------------------------------------------
$apiCheckUserResult = $lgHelper->callRequest("v1/check", ["token" => $jwtobjtosting, "json"=> false]);
if(!$apiCheckUserResult){
	echo $lgHelper->lastError . PHP_EOL;
}
echo $apiCheckUserResult . PHP_EOL. PHP_EOL;


//Generate EIF Template ---------------------------------------------------
$embededIFrame = $lgHelper->buildEIF([
	//"tpl" => "<div>template etc.</div>",
	"tplFile" => getcwd()."/../src/templates/eif.template.php", //fullpath!

	"token" => $jwtstring,
	//"currency" => "₺", //default => "P"
	//"bgColor" => "000", //default => "transparent"
	"frames" => [
		[
			"container" => "lgGameContainer",
			"windowName" => "lgGame",
			"service" => "game"
		]
	]
]);
if(!$embededIFrame){
	echo $lgHelper->lastError . PHP_EOL;
}
echo $embededIFrame . PHP_EOL;
